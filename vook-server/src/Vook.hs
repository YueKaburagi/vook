{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell, DataKinds, TypeOperators, FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unticked-promoted-constructors #-}
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, OverloadedLabels #-}

module Vook where

import Data.Extensible
import Codec.Winery hiding (Tag)
import GHC.TypeLits (symbolVal)

import Data.Default.Class
import qualified Data.Text as Text
import Control.Monad.State.Strict (StateT, runStateT)
import Control.Lens (over, view, use, modifying)
import Test.QuickCheck (Arbitrary(..))
import Test.QuickCheck.Arbitrary.Generic

import Vook.Types
import Vook.State


someFunc = do
  a <- newArtcle "An article" "file://hogehoge"
  lift $ writeFileSerialise "vook.articles" [a]
