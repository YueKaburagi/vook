{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell, DataKinds, TypeOperators, FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unticked-promoted-constructors #-}
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, OverloadedLabels #-}

module Vook.State where

import Prelude hiding (HashMap)
import Data.Extensible
import Codec.Winery (readFileDeserialise, writeFileSerialise, Serialise)

import Data.Default.Class
import Data.Foldable (foldr, maximum)
import qualified Data.Text as Text
import Control.Monad.State.Strict (StateT)
import Control.Lens (over, set, view, use, modifying, assign, Getter, Setter', Lens')
import Data.HashMap.Lazy (HashMap)
import qualified Data.HashMap.Lazy as HashMap
import Data.YAML (FromYAML)
import qualified Data.YAML as YAML
import System.Directory (doesFileExist)

import Vook.Types
import Vook.Types.V0
import Vook.Types.Configure
import Vook.Types.RoughMerge

type VookState = Record
 '[ "currentArticleIndex" :> ArticleIndex
  , "currentSourceIndex" :> SourceIndex
  , "currentSourceGroupIndex" :> SourceGroupIndex
  , "currentPrisetIndex" :> PrisetIndex
  , "articleStore" :> HashMap ArticleIndex Article
  , "sourceStore" :> HashMap SourceIndex Source
  , "sourceGroupStore" :> HashMap SourceGroupIndex SourceGroup
  , "prisetStore" :> HashMap PrisetIndex Priset
  , "configure" :> Configure
  ]
instance Default VookState where
  def = #currentArticleIndex @= 0
        <: #currentSourceIndex @= 0
        <: #currentSourceGroupIndex @= 0
        <: #currentPrisetIndex @= 0
        <: #articleStore @= mempty
        <: #sourceStore @= mempty
        <: #sourceGroupStore @= mempty
        <: #prisetStore @= mempty
        <: #configure @= def
        <: nil

type VookIO = StateT VookState IO


getSafeIndex :: (Eq ix, Enum ix, Hashable ix) => Getter VookState (HashMap ix a) -> Lens' VookState ix -> VookIO ix
getSafeIndex _store _ix = do
  m <- use _store
  ci <- use _ix
  let nci = go m (succ ci)
  assign _ix nci
  use _ix
  where
    go m i | HashMap.member i m = go m (succ i)
           | otherwise = i

getNextArticleIndex :: VookIO ArticleIndex
getNextArticleIndex = getSafeIndex #articleStore #currentArticleIndex
getNextSourceIndex :: VookIO SourceIndex
getNextSourceIndex = getSafeIndex #sourceStore #currentSourceIndex
getNextSourceGroupIndex :: VookIO SourceGroupIndex
getNextSourceGroupIndex = getSafeIndex #sourceGroupStore #currentSourceGroupIndex
getNextPrisetIndex :: VookIO PrisetIndex
getNextPrisetIndex = getSafeIndex #prisetStore #currentPrisetIndex


loadStore :: (Eq k, Ord k, Hashable k, Serialise v)
          => (v -> k)
          -> Setter' VookState (HashMap k v)
          -> Setter' VookState k
          -> FilePath -> VookIO ()
loadStore fk _hashmap _key path = do
  b <- lift $ doesFileExist path
  if b then do
    list <- lift $ readFileDeserialise path
    let hashmap = HashMap.fromList $ fmap (\a -> (fk a, a)) $ list
    assign _hashmap hashmap
    let maxk = maximum $ HashMap.keys hashmap
    assign _key maxk
  else pure ()
saveStore :: Serialise v => Getter VookState (HashMap k v) -> FilePath -> VookIO ()
saveStore _getter path = do
  hashmap <- use _getter
  lift $ writeFileSerialise path $ HashMap.elems hashmap

loadArticleStore0 :: FilePath -> VookIO ()
loadArticleStore0 = loadStore articleIndex #articleStore #currentArticleIndex
saveArticleStore0 :: FilePath -> VookIO ()
saveArticleStore0 = saveStore #articleStore
loadSourceStore0 :: FilePath -> VookIO ()
loadSourceStore0 = loadStore sourceIndex #sourceStore #currentSourceGroupIndex
saveSourceStore0 :: FilePath -> VookIO ()
saveSourceStore0 = saveStore #sourceStore
loadSourceGroupStore0 :: FilePath -> VookIO ()
loadSourceGroupStore0 = loadStore sourceGroupIndex #sourceGroupStore #currentSourceGroupIndex
saveSourceGroupStore0 :: FilePath -> VookIO ()
saveSourceGroupStore0 = saveStore #sourceGroupStore
loadPrisetStore0 :: FilePath -> VookIO ()
loadPrisetStore0 = loadStore prisetIndex #prisetStore #currentPrisetIndex
savePrisetStore0 :: FilePath -> VookIO ()
savePrisetStore0 = saveStore #prisetStore

_configure :: Lens' VookState Configure
_configure = #configure

loadVookState :: VookIO ()
loadVookState = do
  cfg <- use #configure
  loadArticleStore0 (getStoreFilePath #articleStoreFileName cfg)
  loadSourceStore0 (getStoreFilePath #sourceStoreFileName cfg)
  loadSourceGroupStore0 (getStoreFilePath #sourceGroupStoreFileName cfg)
  loadPrisetStore0 (getStoreFilePath #prisetStoreFileName cfg)

saveVookState :: VookIO ()
saveVookState = do
  cfg <- use #configure
  saveArticleStore0 (getStoreFilePath #articleStoreFileName cfg)
  saveSourceStore0 (getStoreFilePath #sourceStoreFileName cfg)
  saveSourceGroupStore0 (getStoreFilePath #sourceGroupStoreFileName cfg)
  savePrisetStore0 (getStoreFilePath #prisetStoreFileName cfg)

saveArticleStore :: VookIO ()
saveArticleStore = saveArticleStore0 . getStoreFilePath #articleStoreFileName =<< use #configure
saveSourceGroupStore :: VookIO ()
saveSourceGroupStore =
 saveSourceGroupStore0 . getStoreFilePath #sourceGroupStoreFileName =<< use #configure


newArtcle :: Title -> URI -> VookIO Article
newArtcle title uri = do
  ix <- getNextArticleIndex
  pure $ Article ix title uri mempty mempty mempty mempty Nothing Nothing mempty
newSource :: Title -> URI -> VookIO Source
newSource title uri = do
  ix <- getNextSourceIndex
  pure $ Source ix title uri mempty mempty mempty Nothing mempty
newSourceGroup :: Title -> VookIO SourceGroup
newSourceGroup title = do
  ix <- getNextSourceGroupIndex
  pure $ SourceGroup ix title mempty mempty mempty
newPriset :: Text -> TagQuery -> VookIO Priset
newPriset name cond = do
  ix <- getNextPrisetIndex
  pure $ Priset ix name cond

oldPrisetToCurrent :: OldPriset -> VookIO Priset
oldPrisetToCurrent opri = do
  let name = oldprisetName opri
  let tq = oldprisrtQuery opri
  newPriset name tq

migratePriset :: OldPriset -> VookIO ()
migratePriset opri = do
  x <- find ((==) (oldprisetName opri) . prisetName) <$> use #prisetStore
  case x of
    Nothing -> do
      p <- oldPrisetToCurrent opri
      modifying #prisetStore (HashMap.insert (prisetIndex p) p)
    Just _ ->
      pure ()
-- if samename deny
-- otherwise allow

oldRecordToArticle :: OldRecord -> VookIO Article
oldRecordToArticle orec = do
  let title = oldrecordName orec
  let uri = oldrecordURL orec
  a <- newArtcle title uri
  pure $
    set _tags (oldrecordTags orec) $
    set _comment (oldrecordComment orec) $
    set _articleThumbnail (fmap head . nonEmpty =<< oldrecordThumbnail orec) $
    a

oldRecordToSource :: OldRecord -> VookIO Source
oldRecordToSource orec = do
  let title = oldrecordName orec
  let uri = oldrecordURL orec
  s <- newSource title uri
  pure $
    set _sourceHome (Just uri) $
    set _tags (oldrecordTags orec) $
    set _comment (oldrecordComment orec) $
    s


migrateOldToCurrent :: OldArticleTypePattern -> OldRecord -> VookIO XIndex
migrateOldToCurrent oatp orec =
  if isArticle (oldrecordURL orec)
    then do
    a <- oldRecordToArticle orec
    t <- find (ifRoughMergeArticle a) <$> use #articleStore
    let x = maybe a (`roughMergeArticle` a) t
    modifying #articleStore (HashMap.insert (view _articleIndex x) x)
    pure $ AIndex (view _articleIndex x)
    else do
    s <- oldRecordToSource orec
    t <- find (ifRoughMergeSource s) <$> use #sourceStore
    let x = maybe s (`roughMergeSource` s) t
    modifying #sourceStore (HashMap.insert (view _sourceIndex x) x)
    pure $ SIndex (view _sourceIndex x)
  where
    sourcePrefix = oatpAsSourcePrefix oatp
    articlePrefix = oatpAsArticlePrefix oatp
    isSource x = any (`Text.isPrefixOf` x) sourcePrefix
    isArticle x = any (`Text.isPrefixOf` x) articlePrefix


oldArticleToSGroup :: OldArticleTypePattern -> OldArticle -> VookIO SourceGroup
oldArticleToSGroup oatp oart = do
  g <- newSourceGroup (oldarticleTitle oart)
  rs <- traverse (migrateOldToCurrent oatp) $ oldarticleRecords oart
  pure $
    set _tags (oldarticleTags oart) $
    set _comment (oldarticleComment oart) $
    foldr ($) g (fmap pushIndex rs)
  where
    pushIndex :: XIndex -> SourceGroup -> SourceGroup
    pushIndex ix = over _sourceGroupContents (ix :)

migrateOldArticle :: OldArticleTypePattern -> OldArticle -> VookIO ()
migrateOldArticle oatp oart = do
  g <- oldArticleToSGroup oatp oart
  t <- find (ifRoughMergeSourceGroup g) <$> use #sourceGroupStore
  let x = maybe g (`roughMergeSourceGroup` g) t
  modifying #sourceGroupStore (HashMap.insert (view _sourceGroupIndex x) x)

appendSourceGoup :: SourceGroup -> VookIO ()
appendSourceGoup sg = do
  modifying #sourceGroupStore (HashMap.insert (view _sourceGroupIndex sg) sg)


loadSingleFromYAML0 :: (FromYAML a, MonadFail m, MonadIO m) => FilePath -> m a
loadSingleFromYAML0 path = do
  bs <- readFileLBS path
  case YAML.decode1 bs of
    Left err -> fail $ show err
    Right c -> pure c

loadOldCollection :: (MonadFail m, MonadIO m) => FilePath -> m OldCollection
loadOldCollection = loadSingleFromYAML0
loadOldArticleTypePattern :: (MonadFail m, MonadIO m) => FilePath -> m OldArticleTypePattern
loadOldArticleTypePattern = loadSingleFromYAML0
loadOldPrisets :: (MonadFail m, MonadIO m) => FilePath -> m [OldPriset]
loadOldPrisets = loadSingleFromYAML0
