{-# LANGUAGE DataKinds, TypeOperators #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}

module Vook.API where

import Prelude hiding (withState)

import Control.Monad.Cont (ContT, runContT)
import Data.Fallible (Fallible(..), (!??), exit)
import qualified Data.Text as Text
import Data.Text.Read (decimal)
import qualified Data.List as List
import Servant.API
import Servant.Server (Server, ServerT, Handler, serve)
import Servant.Server.StaticFiles (serveDirectoryWebApp)
import Network.Wai (Application)
import Network.Wai.Handler.Warp (run, Port)
import Data.Aeson hiding (Options)
import qualified Data.HashMap.Strict as HashMap
import Control.Lens (view, use, at, over, set, assign)

import Vook.Types.V1
import Vook.Types (Priset(..), Source, SourceGroup, SourceGroupIndex, _tags, _title, _comment, sourceGroupIndex)
import Vook.State
import Vook.Types.RoughMerge (roughMergeSourceGroup)

type WithCORS a = Headers '[ Header "Access-Control-Allow-Origin" String
                           ] a
type WithCORSPreflight a = Headers '[ Header "Access-Control-Allow-Origin" String
                                    , Header "Access-Control-Allow-Methods" String
                                    , Header "Access-Control-Allow-Headers" String
                                    ] a
type Options = Verb 'OPTIONS 200


type VookAPI = "entries" :> Get '[JSON] (WithCORS [ClientSourceGroup])
             :<|> "prisets" :> Get '[JSON] (WithCORS [Priset])
             :<|> "sgroup" :> "modify" :> Options '[JSON] (WithCORSPreflight ())
             :<|> "sgroup" :> "modify" :> ReqBody '[JSON] ClientModifySGroup :> Patch '[JSON] (WithCORS (ServerResponse ClientSourceGroup))
             :<|> "sgroup" :> "merge" :> Options '[JSON] (WithCORSPreflight ())
             :<|> "sgroup" :> "merge" :> ReqBody '[JSON] ClientMergeSGroup :> Patch '[JSON] (WithCORS (ServerResponse ClientSourceGroup))
             :<|> "sgroup" :> "remove" :> Options '[JSON] (WithCORSPreflight ())
             :<|> "sgroup" :> "remove" :> ReqBody '[JSON] ClientRemoveSGroup :> Patch '[JSON] (WithCORS (ServerResponse Text))
             :<|> "static" :> Raw

server :: TVar VookState -> Server VookAPI
server v = entries v
           :<|> prisets v
           :<|> (pure $ addHeader "*" $ addHeader "PATCH" $ addHeader "Content-Type" $ ())
           :<|> modifySGroup v
           :<|> (pure $ addHeader "*" $ addHeader "PATCH" $ addHeader "Content-Type" $ ())
           :<|> mergeSGroup v
           :<|> (pure $ addHeader "*" $ addHeader "PATCH" $ addHeader "Content-Type" $ ())
           :<|> removeSGroup v
           :<|> serveDirectoryWebApp "static"


entries :: TVar VookState -> Handler (WithCORS [ClientSourceGroup])
entries v = do
  state <- atomically $ readTVar v
  let sgs = HashMap.elems (view #sourceGroupStore state)
  pure $ addHeader "*" $
    fSourceGroup (view #articleStore state) (view #sourceStore state) <$> sgs

prisets :: TVar VookState -> Handler (WithCORS [Priset])
prisets v = do
  state <- atomically $ readTVar v
  let pris = HashMap.elems (view #prisetStore state)
  pure $ addHeader "*" $
    sortWith prisetName pris

modifySGroup :: TVar VookState -> ClientModifySGroup -> Handler (WithCORS (ServerResponse ClientSourceGroup))
modifySGroup v query = do
  m <- atomically $ withState v $ execContT $ do
    let idx_ = cqaIndex query
    (idx,_) <- lift $ readIndex "SourceGroup" idx_
    a <- use (#sourceGroupStore . at idx) `orFallWithM` (exit . lift . failExceptT $ "No such index: " <> Text.unpack idx_ <> ".")
    let sg = genModifier query a
    assign (#sourceGroupStore . at idx) (Just sg)
    arts <- use #articleStore
    srcs <- use #sourceStore
    pure $ fSourceGroup arts srcs sg
  aftercare v saveSourceGroupStore
  pure $ addHeader "*" $ either (SRFailed . Text.pack) SRSucceed m
  where
    genModifier :: ClientModifySGroup -> SourceGroup -> SourceGroup
    genModifier q sg =
      foldr ($) sg
      [ maybe id modifyTitle (cqaTitle q)
      , maybe id modifyTags (cqaTags q)
      , maybe id modifyComment (cqaComment q)
      ]
    modifyTitle = set _title
    modifyTags = set _tags
    modifyComment c = set _comment $ bool (Just c) Nothing (Text.null c)

mergeSGroup :: TVar VookState -> ClientMergeSGroup -> Handler (WithCORS (ServerResponse ClientSourceGroup))
mergeSGroup v query = do
  m <- atomically $ withState v $ execContT $ do
    let oidx_ = cmsgOriginIndex query
    let tidx_ = cmsgTargetIndex query
    (oidx, _) <- lift $ readIndex "origin" oidx_
    (tidx, _) <- lift $ readIndex "target" tidx_
    origin <- use (#sourceGroupStore . at oidx) `orFallWithM` (exit . lift . failExceptT $ "[origin] No such index: " <> Text.unpack oidx_ <> ".")
    target <- use (#sourceGroupStore . at tidx) `orFallWithM` (exit . lift . failExceptT $ "[target] No such index: " <> Text.unpack tidx_ <> ".")
    let sg = roughMergeSourceGroup origin target
    assign (#sourceGroupStore . at oidx) (Just sg)
    if cmsgDeleteTargetIfSucceed query
      then assign (#sourceGroupStore . at tidx) Nothing
      else pure ()
    arts <- use #articleStore
    srcs <- use #sourceStore
    pure $ fSourceGroup arts srcs sg
  aftercare v saveSourceGroupStore
  pure $ addHeader "*" $ either (SRFailed . Text.pack) SRSucceed m

removeSGroup :: TVar VookState -> ClientRemoveSGroup -> Handler (WithCORS (ServerResponse Text))
removeSGroup v query = do
  m <- atomically $ withState v $ execContT $ do
    let idx_ = crsgIndex query
    (idx, _) <- lift $ readIndex "SourceGroup" idx_
    sg <- use (#sourceGroupStore . at idx) `orFallWithM` (exit . lift . failExceptT $ "No such index: " <> Text.unpack idx_ <> ".")
    assign (#sourceGroupStore . at idx) Nothing
    pure $ Text.pack $ show $ sourceGroupIndex sg
  aftercare v saveSourceGroupStore
  pure $ addHeader "*" $ either (SRFailed . Text.pack) SRSucceed m



decorateInvalidIndex :: String -> Text -> String -> String
decorateInvalidIndex p idx msg =
  "Invalid " <> p <> " index: '" <> Text.unpack idx <> "'. " <> msg

readIndex :: (MonadTrans t, Monad m) => String -> Text -> t (ExceptT String m) (Integer, Text)
readIndex p idx = lift $ ExceptT . pure $ first (decorateInvalidIndex p idx) $ decimal idx

aftercare :: MonadIO m => TVar s -> StateT s IO a -> m ()
aftercare v body = do
  state <- liftIO $ readTVarIO v
  void $ liftIO $ evalStateT body state

withState :: TVar s -> StateT s (ExceptT e STM) a -> STM (Either e a)
withState v body = do
  state <- readTVar v
  r <- runExceptT $ runStateT body state
  mapM (\(a, s) -> writeTVar v s >> pure a) r

execContT :: Applicative m => ContT r m r -> m r
execContT = flip runContT pure

failExceptT :: Applicative m => e -> ExceptT e m a
failExceptT = ExceptT . pure . Left

orFallWithM :: (Monad m, Fallible t) => m (t a) -> m a -> m a
orFallWithM = (!??)


userAPI :: Proxy VookAPI
userAPI = Proxy

runAsServer :: Port -> TVar VookState -> IO ()
runAsServer port mega = run port (serve userAPI $ server mega)
