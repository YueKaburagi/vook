
module Vook.Types.Lens where

import Control.Lens (set, FieldNamer, makeLensesWith, LensRules, lensRules, lensField)
import Control.Lens.Combinators (DefName(TopName))

import Language.Haskell.TH (nameBase, mkName)

underscoringNamer :: FieldNamer
underscoringNamer _ _ n =
  case nameBase n of
    x:xs -> [TopName (mkName ( '_':x:xs ))]
    _ -> []

myLensRule :: LensRules
myLensRule = set lensField underscoringNamer lensRules 

