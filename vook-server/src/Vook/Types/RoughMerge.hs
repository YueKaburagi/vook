{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DeriveGeneric, GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell, DataKinds, TypeOperators, FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unticked-promoted-constructors #-}
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, OverloadedLabels #-}


module Vook.Types.RoughMerge ( roughMergeArticle
                             , roughMergeSource
                             , roughMergeSourceGroup
                             , ifRoughMergeArticle
                             , ifRoughMergeSource
                             , ifRoughMergeSourceGroup
                             ) where

import Data.Default.Class
import qualified Data.List as List
import qualified Data.Text as Text
import Control.Monad.State.Strict (StateT, runStateT)
import Control.Lens (set, use, over, view, modifying, Lens',  makeLensesWith, Wrapped(..), iso)
import Test.QuickCheck (Arbitrary(..))
import Test.QuickCheck.Arbitrary.Generic
import Data.Aeson (Value(..), ToJSON(..), (.=), object)
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap

import Vook.Types


data ForallEqLens' s where
  ForallEqLens' :: Eq a => Lens' s a -> ForallEqLens' s
-- [ForallEqLens' s] as like [forall a . Eq a => Lens' s a]

data ForallRoughMergeLens' s where
  ForallRoughMergeLens' :: Lens' s a -> (a -> a -> a) -> ForallRoughMergeLens' s

ifRoughMerge :: [ForallEqLens' s] -> s -> s -> Bool
ifRoughMerge lq a b = all (\f -> f a b) $ fmap (\(ForallEqLens' lens) -> (==) `on` view lens) lq

roughMerge :: [ForallRoughMergeLens' s] -> s -> s -> s
roughMerge lq a b = foldr ($) a $ fmap (merge b) lq
  where
    merge :: s -> ForallRoughMergeLens' s -> s -> s
    merge y (ForallRoughMergeLens' _lens f) = over _lens (f (view _lens y))

useLarger :: (Functor f, Applicative f, Alternative f, Ord a)
          => f a -> f a -> f a
useLarger a b = (max <$> a <*> b) <|> a <|> b
useSmaller :: (Functor f, Applicative f, Alternative f, Ord a)
           => f a -> f a -> f a
useSmaller a b = (min <$> a <*> b) <|> a <|> b

roughMergeArticle :: Article -> Article -> Article
roughMergeArticle =
  roughMerge
  [ ForallRoughMergeLens' _articleThumbnail (<|>)
  , ForallRoughMergeLens' _articleImages List.union
  , ForallRoughMergeLens' _articleTags List.union
  , ForallRoughMergeLens' _articleTimestamp useLarger
  , ForallRoughMergeLens' _articleLastUpdated useLarger
  , ForallRoughMergeLens' _articleFirstDetected useSmaller
  , ForallRoughMergeLens' _articleComment (<|>)
  ]
roughMergeSource :: Source -> Source -> Source
roughMergeSource =
  roughMerge
  [ ForallRoughMergeLens' _sourceHome (<|>)
  , ForallRoughMergeLens' _sourceContents List.union
  , ForallRoughMergeLens' _sourceTags List.union
  , ForallRoughMergeLens' _sourceLastCrawled useLarger
  , ForallRoughMergeLens' _sourceComment (<|>)
  ]
roughMergeSourceGroup :: SourceGroup -> SourceGroup -> SourceGroup
roughMergeSourceGroup =
  roughMerge
  [ ForallRoughMergeLens' _sourceGroupTags List.union
  , ForallRoughMergeLens' _sourceGroupContents List.union
  , ForallRoughMergeLens' _sourceGroupComment (<|>)
  ]

ifRoughMergeArticle :: Article -> Article -> Bool
ifRoughMergeArticle =
  ifRoughMerge [ ForallEqLens' _articleTitle
               , ForallEqLens' _articleURI
               ]
ifRoughMergeSource :: Source -> Source -> Bool
ifRoughMergeSource =
  ifRoughMerge [ ForallEqLens' _sourceTitle
               , ForallEqLens' _sourceURI
               ]
ifRoughMergeSourceGroup :: SourceGroup -> SourceGroup -> Bool
ifRoughMergeSourceGroup =
  ifRoughMerge [ ForallEqLens' _sourceGroupTitle
               , ForallEqLens' _sourceGroupContents
               ]
