{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Vook.Types.V0 where

import Data.Default.Class
import qualified Data.Text as Text
import Data.YAML (FromYAML(..), (.:), (.:?), (.!=), withMap, withInt, withFloat, Parser)
import Control.Monad.State.Strict (StateT, runStateT)
import Control.Lens (set, use, modifying, Lens',  makeLensesWith)
import Test.QuickCheck (Arbitrary(..))
import Test.QuickCheck.Arbitrary.Generic


import Vook.Types


-- orphan
instance FromYAML Tag where
  parseYAML x = Tag <$> (parseYAML x <|> intLike x)
    where
      intLike = withInt "Tag^int" (pure . Text.pack . show)
instance FromYAML Title where
  parseYAML x = Title <$> (parseYAML x <|> intLike x <|> floatLike x)
    where
      intLike = withInt "Title^int" (pure . Text.pack . show)
      floatLike = withFloat "Title^float" (pure . Text.pack . show)

instance FromYAML TagQuery where
  parseYAML a = oneOf $ fmap (a &) $ tqTqg0 :| [tqTagS, tqNot, tqAll, tqAny]
    where
      tqTqg0 x = TQTagStrict <$> parseYAML x
      tqTagS = withMap "TagS" $ \m -> TQTagStrict <$> m .: "tag"
      tqNot = withMap "Not" $ \m -> TQNot <$> m .: "not"
      tqAll = withMap "All" $ \m -> TQAll <$> m .: "all"
      tqAny = withMap "Any" $ \m -> TQAny <$> m .: "any"

data OldPriset = OldPriset
  { oldprisetName :: !Text
  , oldprisrtQuery :: !TagQuery
  } deriving (Show, Eq, Ord, Generic)
instance FromYAML OldPriset where
  parseYAML = withMap "OldPriset" $ \m -> do
    name <- m .: "name"
    pred <- m .:? "pred" .!= TQTagStrict (Tag name)
    pure $ OldPriset name pred

-- | bookmark-und の初期タイプのyaml構造の Lebel 2
--  ほぼbookmark
data OldRecord = OldRecord
  { oldrecordName :: !Title
  , oldrecordURL :: !URI
  , oldrecordTags :: ![Tag]
  , oldrecordComment :: !(Maybe Text)
  , oldrecordThumbnail :: !(Maybe [URI])
  } deriving (Show, Eq, Ord, Generic)
instance FromYAML OldRecord where
  parseYAML = withMap "OldRecord" $ \m ->
    OldRecord
      <$> m .: "name"
      <*> m .: "url"
      <*> m .:? "tags" .!= []
      <*> m .:? "comment"
      <*> m .:? "thumbnail"

-- | bookmark-und の初期タイプのyaml構造の Level 1
--  ひとまとまりの source|article 群
data OldArticle = OldArticle
  { oldarticleTitle :: !Title
  , oldarticleRecords :: ![OldRecord]
  , oldarticleTags :: ![Tag]
  , oldarticleComment :: !(Maybe Text)
  } deriving (Show, Eq, Ord, Generic)
instance FromYAML OldArticle where
  parseYAML = withMap "OldArticle" $ \m ->
    OldArticle
      <$> m .: "title"
      <*> m .: "records"
      <*> m .:? "tags" .!= []
      <*> m .:? "comment"


-- | prefix pattern match 用のyamlの capturer
--  bookmark-und のresourceを vook の型に落とし込むためのもの
data OldArticleTypePattern = OldArticleTypePattern
  { oatpAsSourcePrefix :: ![Text]
  , oatpAsArticlePrefix :: ![Text]
  } deriving (Show, Eq, Ord, Generic)
instance FromYAML OldArticleTypePattern where
  parseYAML = withMap "OATP" $ \m ->
    OldArticleTypePattern
      <$> m .:? "source" .!= []
      <*> m .:? "article" .!= []


-- | bookmark-und の初期タイプのyaml構造の Level 0
--  最外殻にcollectionを要求するので、それに合わせるため
data OldCollection = OldCollection
  { oldCollection :: [OldArticle]
  } deriving (Show, Eq, Ord, Generic)
instance FromYAML OldCollection where
  parseYAML = withMap "OldCollection" $ \m ->
    OldCollection <$> m .: "collection"
