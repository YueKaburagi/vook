{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedLabels #-}

module Vook.Types.V1 where

import Data.Default.Class
import qualified Data.Text as Text
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Control.Monad.State.Strict (StateT, runStateT)
import Control.Lens (set, use, modifying, Lens',  makeLensesWith, view)
import Test.QuickCheck (Arbitrary(..))
import Test.QuickCheck.Arbitrary.Generic
import Data.Aeson

import Vook.Types

data ServerResponse a = SRFailed !Text
                      | SRSucceed a
                      deriving (Show, Eq, Ord, Generic)
instance ToJSON a => ToJSON (ServerResponse a) where
  toJSON (SRFailed err) = object [ "failed" .= err ]
  toJSON (SRSucceed a) = object [ "succeed" .= a ]

data ClientModifySGroup = ClientModifySGroup
  { cqaIndex :: !Text
  , cqaTitle :: !(Maybe Title)
  , cqaTags :: !(Maybe [Tag])
  , cqaComment :: !(Maybe Text)
  } deriving (Show, Eq, Ord, Generic)
instance FromJSON ClientModifySGroup where
  parseJSON = withObject "ClientModifySGroup" $ \v ->
    ClientModifySGroup
    <$> v .: "index"
    <*> v .:? "title"
    <*> v .:? "tags"
    <*> v .:? "comment"

data ClientMergeSGroup = ClientMergeSGroup
  { cmsgOriginIndex :: !Text
  , cmsgTargetIndex :: !Text
  , cmsgDeleteTargetIfSucceed :: !Bool
  } deriving (Show, Eq, Ord, Generic)
instance FromJSON ClientMergeSGroup where
  parseJSON = withObject "ClientMergeSGroup" $ \v ->
    ClientMergeSGroup
    <$> v .: "origin"
    <*> v .: "target"
    <*> v .: "delete_target_if_succeed"

data ClientRemoveSGroup = ClientRemoveSGroup
  { crsgIndex :: !Text
  } deriving (Show, Eq, Ord, Generic)
instance FromJSON ClientRemoveSGroup where
  parseJSON = withObject "ClientRemoveSGroup" $ \v ->
    ClientRemoveSGroup <$> v .: "index"




data ClientArticle = ClientArticle
  { clientArticleIndex :: !Text
  , clientArticleTitle :: !Title
  , clientArticleURI :: !URI
  , clientArticleTags :: ![Tag]
  , clientArticleThumbnail :: !(Maybe URI)
  , clientArticleComment :: !(Maybe Text)
  } deriving (Show, Eq, Ord, Generic)
instance ToJSON ClientArticle where
  toJSON a = object
    [ "index" .= clientArticleIndex a
    , "type" .= Text.pack "article"
    , "title" .= clientArticleTitle a
    , "uri" .= clientArticleURI a
    , "tags" .= clientArticleTags a
    , "thumbnail" .= clientArticleThumbnail a
    , "comment" .= clientArticleComment a
    ]
fArticle :: Article -> ClientArticle
fArticle a =
  ClientArticle
  (Text.pack $ show $ articleIndex a)
  (articleTitle a)
  (articleURI a)
  (articleTags a)
  (articleThumbnail a)
  (articleComment a)

data ClientSource = ClientSource
  { clientSourceIndex :: !Text
  , clientSourceTitle :: !Title
  , clientSourceURI :: !URI
  , clientSourceHome :: !URI
  , clientSourceArticles :: ![ClientArticle]
  , clientSourceTags :: ![Tag]
  , clientSourceComment :: !(Maybe Text)
  } deriving (Show, Eq, Ord, Generic)
instance ToJSON ClientSource where
  toJSON a = object
    [ "index" .= clientSourceIndex a
    , "type" .= Text.pack "source"
    , "title" .= clientSourceTitle a
    , "uri" .= clientSourceURI a
    , "home" .= clientSourceHome a
    , "articles" .= clientSourceArticles a
    , "tags" .= clientSourceTags a
    , "comment" .= clientSourceComment a
    ]
fSource :: HashMap ArticleIndex Article -> Source -> ClientSource
fSource am s =
  let articles = catMaybes $ (`HashMap.lookup` am) <$> sourceContents s
  in
    ClientSource
    (Text.pack $ show $ sourceIndex s)
    (sourceTitle s)
    (sourceURI s)
    (fromMaybe (sourceURI s) $ sourceHome s)
    (fmap fArticle articles)
    (sourceTags s)
    (sourceComment s)

data ClientXContent = CAC ClientArticle
                    | CSC ClientSource
                    deriving (Show, Eq, Ord, Generic)
instance ToJSON ClientXContent where
  toJSON (CAC a) = toJSON a
  toJSON (CSC a) = toJSON a
  -- They contains 'type' field.

data ClientSourceGroup = ClientSourceGroup
  { clientSourceGroupIndex :: !Text
  , clientSourceGroupTitle :: !Title
  , clientSourceGroupContents :: ![ClientXContent]
  , clientSourceGroupTags :: ![Tag]
  , clientSourceGroupComment :: !(Maybe Text)
  } deriving (Show, Eq, Ord, Generic)
instance ToJSON ClientSourceGroup where
  toJSON a = object
    [ "index" .= clientSourceGroupIndex a
    , "title" .= clientSourceGroupTitle a
    , "contents" .= clientSourceGroupContents a
    , "tags" .= clientSourceGroupTags a
    , "comment" .= clientSourceGroupComment a
    ]
fSourceGroup :: HashMap ArticleIndex Article -> HashMap SourceIndex Source -> SourceGroup -> ClientSourceGroup
fSourceGroup am sm sg =
  ClientSourceGroup
  (Text.pack $ show $ sourceGroupIndex sg)
  (sourceGroupTitle sg)
  (catMaybes $ flat <$> sourceGroupContents sg)
  (sourceGroupTags sg)
  (sourceGroupComment sg)
  where
    flat (AIndex ix) = CAC . fArticle <$> HashMap.lookup ix am
    flat (SIndex ix) = CSC . fSource am <$> HashMap.lookup ix sm


data ElmRecord = ElmRecord
  { elmRecordIndex :: !Text
  , elmRecordTitle :: !Title
  , elmRecordURL :: !URI
  , elmRecordTags :: ![Tag]
  , elmRecordComment :: !(Maybe Text)
  , elmRecordThumbnails :: ![URI]
  } deriving (Show, Eq, Ord, Generic)

instance ToJSON ElmRecord where
  toJSON a = object
    [ "index" .= elmRecordIndex a
    , "name" .= elmRecordTitle a
    , "url" .= elmRecordURL a
    , "tags" .= elmRecordTags a
    , "comment" .= elmRecordComment a
    , "thumbnail" .= elmRecordThumbnails a
    ]

data ElmArticle = ElmArticle
  { elmArticleIndex :: !Text
  , elmArticleTitle :: !Title
  , elmArticleRecords :: ![ElmRecord]
  , elmArticleTags :: ![Tag]
  , elmArticleComment :: !(Maybe Text)
  } deriving (Show, Eq, Ord, Generic)

instance ToJSON ElmArticle where
  toJSON a = object
    [ "index" .= elmArticleIndex a
    , "title" .= elmArticleTitle a
    , "records" .= elmArticleRecords a
    , "tags" .= elmArticleTags a
    , "comment" .= elmArticleComment a
    ]


class ToElmRecord a where
  toElmRecord :: a -> ElmRecord
instance ToElmRecord Article where
  toElmRecord a =
    ElmRecord
    (Text.pack $ show $ articleIndex a)
    (view _title a)
    (articleURI a)
    (view _tags a)
    (view _comment a)
    (toList $ articleThumbnail a)
instance ToElmRecord Source where
  toElmRecord s =
    ElmRecord
    (Text.pack $ show $ sourceIndex s)
    (view _title s)
    (fromMaybe (sourceURI s) (sourceHome s))
    (view _tags s)
    (view _comment s)
    mempty -- concat . fmap (take one or zero . articleThumbnails) . sourceContents


{-# DEPRECATED collaborate "use instead fSourceGroup" #-}
collaborate :: HashMap ArticleIndex Article
            -> HashMap SourceIndex Source
            -> SourceGroup -> ElmArticle
collaborate arts srcs sg =
  let
    ma = []--catMaybes $ flip HashMap.lookup arts <$> sourceGroupArticles sg
    ms = []--catMaybes $ flip HashMap.lookup srcs <$> sourceGroupSources sg
    records = []--fmap toElmRecord ma <> fmap toElmRecord ms
  in
    ElmArticle
    (Text.pack $ show $ sourceGroupIndex sg)
    (view _title sg)
    records
    (view _tags sg)
    (view _comment sg)
