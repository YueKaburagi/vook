{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell, DataKinds, TypeOperators, FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unticked-promoted-constructors #-}
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, OverloadedLabels #-}

module Vook.Types.Configure where

import Data.Extensible

import Data.Default.Class
import Data.Foldable (foldr, maximum)
import qualified Data.Text as Text
import Control.Monad.State.Strict (StateT)
import Control.Lens (over, set, view, use, modifying, assign, Getter, Setter')
import Data.HashMap.Lazy (HashMap)
import Data.YAML (FromYAML)
import qualified Data.YAML as YAML
import System.FilePath.Posix ((</>)) -- alter S.FP.Windows

import Vook.Types


type Configure = Record
 '[ "storeDirectory" :> FilePath
  , "articleStoreFileName" :> FilePath
  , "sourceStoreFileName" :> FilePath
  , "sourceGroupStoreFileName" :> FilePath
  , "prisetStoreFileName" :> FilePath
  ]

instance Default Configure where
  def = #storeDirectory @= "temporary"
        <: #articleStoreFileName @= "store.article"
        <: #sourceStoreFileName @= "store.source"
        <: #sourceGroupStoreFileName @= "store.sourcegroup"
        <: #prisetStoreFileName @= "store.priset"
        <: nil


getStoreFilePath :: Getter Configure FilePath -> Configure -> FilePath
getStoreFilePath _fileName cfg =
  view #storeDirectory cfg </> view _fileName cfg
