{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DeriveGeneric, GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell, DataKinds, TypeOperators, FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unticked-promoted-constructors #-}
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, OverloadedLabels #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE ApplicativeDo #-}

module Vook.Types where

import Relude.Extra.Foldable1 (foldl1')
import Codec.Winery hiding (Tag)

import Data.Default.Class
import Data.Foldable (foldl)
import qualified Data.Text as Text
import Control.Monad.State.Strict (StateT, runStateT)
import Control.Lens (set, use, view, modifying, Lens',  makeLensesWith, Wrapped(..), iso, _Unwrapped')
import Test.QuickCheck (Arbitrary(..))
import Test.QuickCheck.Arbitrary.Generic
import Data.Aeson (Value(..), ToJSON(..), (.=), object, FromJSON(..), (.:), (.:?), (.!=), withObject)
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.Time.Clock (UTCTime(..))
import Data.Time.Calendar (Day(..))

import Vook.Types.Lens

type ArticleIndex = Integer
type SourceIndex = Integer
type SourceGroupIndex = Integer
type PrisetIndex = Integer
type URI = Text

newtype Tag = Tag Text deriving (Show, Eq, Ord, Generic, Arbitrary, Serialise, ToJSON, FromJSON)
newtype Title = Title Text deriving (Show, Eq, Ord, Generic, Arbitrary, Serialise, IsString, ToJSON, FromJSON)
instance Wrapped Tag where
  type Unwrapped Tag = Text
  _Wrapped' = iso (\(Tag a) -> a) Tag
instance Wrapped Title where
  type Unwrapped Title = Text
  _Wrapped' = iso (\(Title a) -> a) Title

data TagQuery = TQTag Text
              | TQTagStrict Tag
              | TQUri URI
              | TQText Text
              | TQTextStrict Text
              | TQNot TagQuery
              | TQAll [TagQuery]
              | TQAny [TagQuery]
              deriving (Show, Eq, Ord, Generic)
instance ToJSON TagQuery where
  toJSON (TQTag str) = object [ "tag_partial" .= str ]
  toJSON (TQTagStrict tag) = object [ "tag" .= tag ]
  toJSON (TQUri uri) = object [ "uri" .= uri ]
  toJSON (TQText str) = object [ "text" .= str ]
  toJSON (TQTextStrict str) = object [ "text_strict" .= str ]
  toJSON (TQNot tq) = object [ "not" .= tq ]
  toJSON (TQAll l) = object [ "all" .= l ]
  toJSON (TQAny l) = object [ "any" .= l ]
instance FromJSON TagQuery where
  parseJSON = withObject "TagQuery" $ \v ->
    oneOf $ fmap (v &) $ tqTag :| [tqTagS, tqUri, tqText, tqTextS, tqNot, tqAll, tqAny]
    where
      tqTag v = TQTag <$> v .: "tag_partial"
      tqTagS v = TQTagStrict <$> v .: "tag"
      tqUri v = TQUri <$> v .: "uri"
      tqText v = TQText <$> v .: "text"
      tqTextS v = TQTextStrict <$> v .: "text_strict"
      tqNot v = TQNot <$> v .: "not"
      tqAll v = TQAll <$> v .: "all"
      tqAny v = TQAny <$> v .: "any"

oneOf :: Alternative m => NonEmpty (m a) -> m a
oneOf = foldl1' (<|>)


-- orphan instance
instance Arbitrary Text where
  arbitrary = Text.pack <$> arbitrary
instance Arbitrary UTCTime where
  arbitrary = UTCTime <$> fmap ModifiedJulianDay arbitrary <*> fmap fromInteger arbitrary


data Priset = Priset
  { prisetIndex :: !PrisetIndex
  , prisetName :: !Text
  , prisetQuery :: !TagQuery
  } deriving (Show, Eq, Ord, Generic)
makeLensesWith myLensRule ''Priset
instance ToJSON Priset where
  toJSON p = object
    [ "index" .= Text.pack ( show ( prisetIndex p ))
    , "name" .= prisetName p
    , "query" .= prisetQuery p
    ]

data Article = Article
  { articleIndex :: !ArticleIndex
  , articleTitle :: !Title
  , articleURI :: !URI
  , articleThumbnail :: !(Maybe URI)
  , articleImages :: ![URI]
  , articleTags :: ![Tag]
  , articleTimestamp :: !(Maybe Text)
  , articleLastUpdated :: !(Maybe UTCTime)
  , articleFirstDetected :: !(Maybe UTCTime)
  , articleComment :: !(Maybe Text)
  } deriving (Show, Eq, Ord, Generic)
instance Arbitrary Article where
  arbitrary = genericArbitrary
  shrink = genericShrink
makeLensesWith myLensRule ''Article

data Source = Source
  { sourceIndex :: !SourceIndex
  , sourceTitle :: !Title
  , sourceURI :: !URI
  , sourceHome :: !(Maybe URI)
  , sourceContents :: ![ArticleIndex]
  , sourceTags :: ![Tag]
  , sourceLastCrawled :: !(Maybe UTCTime)
  , sourceComment :: !(Maybe Text)
  } deriving (Show, Eq, Ord, Generic)
makeLensesWith myLensRule ''Source

data XIndex = AIndex ArticleIndex
            | SIndex SourceIndex
            deriving (Show, Eq, Ord, Generic)


data SourceGroup = SourceGroup
  { sourceGroupIndex :: !SourceGroupIndex
  , sourceGroupTitle :: !Title
  , sourceGroupContents :: ![XIndex]
  , sourceGroupTags :: ![Tag]
  , sourceGroupComment :: !(Maybe Text)
  } deriving (Show, Eq, Ord, Generic)
makeLensesWith myLensRule ''SourceGroup

class HasTitle a where
  _title :: Lens' a Title
instance HasTitle Article where _title = _articleTitle
instance HasTitle Source where _title = _sourceTitle
instance HasTitle SourceGroup where _title = _sourceGroupTitle

class HasTags a where
  _tags :: Lens' a [Tag]
instance HasTags Article where _tags = _articleTags
instance HasTags Source where _tags = _sourceTags
instance HasTags SourceGroup where _tags = _sourceGroupTags

class HasComment a where
  _comment :: Lens' a (Maybe Text)
instance HasComment Article where _comment = _articleComment
instance HasComment Source where _comment = _sourceComment
instance HasComment SourceGroup where _comment = _sourceGroupComment


instance Serialise TagQuery where
  bundleSerialise = bundleVariant $ const $ buildExtractor $
    ( "TQTag", TQTag ) `extractConstructor`
    ( "TQTagS", TQTagStrict . Tag ) `extractConstructor`
    ( "TQUri", TQUri ) `extractConstructor`
    ( "TQText", TQText ) `extractConstructor`
    ( "TQTextS", TQTextStrict ) `extractConstructor`
    ( "TQNot", TQNot ) `extractConstructor`
    ( "TQAll", TQAll ) `extractConstructor`
    ( "TQAny", TQAny ) `extractConstructor`
    extractVoid

instance Serialise Priset where
  bundleSerialise = bundleRecord $ const $ buildExtractor $
    Priset
      <$> extractField "index"
      <*> extractField "name"
      <*> extractField "query"


instance Serialise Article where
  bundleSerialise = bundleRecord $ const $ buildExtractor $ do
    index <- extractField "articleIndex"
    title <- extractField "articleTitle"
    uri <- extractField "articleURI"
    thumbnail <- extractField "articleThumbnail" <|> (fmap head . nonEmpty <$> extractField "articleThumbnails")
    images <- extractField "articleImages"
    tags <- extractField "articleTags"
    timestamp <- extractField "articleTimestamp"
    last <- extractField "articleLastUpdated" <|> pure Nothing
    first <- extractField "articleFirstDetected" <|> pure Nothing
    comment <- extractField "articleComment" <|> pure Nothing
    pure $ Article index title uri thumbnail images tags timestamp last first comment


instance Serialise Source where
  bundleSerialise = bundleRecord $ const $ buildExtractor $ do
    index <- extractField "sourceIndex"
    title <- extractField "sourceTitle"
    uri <- extractField "sourceURI"
    home <- extractField "sourceHome"
    articles <- extractField "sourceContents"
    tags <- extractField "sourceTags"
    last <- extractField "sourceLastCrawled" <|> pure Nothing
    comment <- extractField "sourceComment" <|> pure Nothing
    pure $ Source index title uri home articles tags last comment

instance Serialise XIndex where
  bundleSerialise = bundleVariant $ const $ buildExtractor $
    ( "A", AIndex ) `extractConstructor`
    ( "S", SIndex ) `extractConstructor`
    extractVoid

instance Serialise SourceGroup where
  bundleSerialise = bundleRecord $ const $ buildExtractor $ do
    index <- extractField "sourceGroupIndex"
    title <- extractField "sourceGroupTitle"
    contents <- oldStek <|> extractField "sourceGroupContents" <|> pure []
    tags <- extractField "sourceGroupTags"
    comment <- extractField "sourceGroupComment"
    pure $ SourceGroup index title contents tags comment
      where
        oldStek = (\x y -> fmap AIndex x <> fmap SIndex y) <$> extractField "sourceGroupArticles" <*> extractField "sourceGroupSources"


class ToV0JSON a where
  toV0JSON :: a -> Value

instance ToV0JSON Article where
  toV0JSON a =
    object $
    [ "name" .= view (_title . _Wrapped') a
    , "url" .= view _articleURI a
    , "tags" .= view _tags a
    ] <> maybe [] (pure . ("comment" .=)) (view _comment a)

instance ToV0JSON Source where
  toV0JSON a =
    object $
    [ "name" .= view (_title . _Wrapped') a
    , "url" .= fromMaybe (view _sourceURI a) (view _sourceHome a)
    , "tags" .= view _tags a
    ] <> maybe [] (pure . ("comment" .=)) (view _comment a)


toV0JSONSourceGroup :: HashMap ArticleIndex Article
                    -> HashMap SourceIndex Source
                    -> SourceGroup -> Value
toV0JSONSourceGroup arts srcs a =
  let
    rcs = catMaybes $ flat <$> sourceGroupContents a
    in
    object $
    [ "title" .= view (_title . _Wrapped') a
    , "tags" .= view _tags a
    , "records" .= rcs
    ] <> maybe [] (pure . ("comment" .=)) (view _comment a)
  where
    flat (AIndex ix) = toV0JSON <$> HashMap.lookup ix arts
    flat (SIndex ix) = toV0JSON <$> HashMap.lookup ix srcs


encloseCollection :: ToJSON a => a -> Value
encloseCollection v =
  object [ "collection" .= v ]
