module Test.Vook where

import Codec.Winery (serialise, deserialise)

import Vook.Types



prop_article :: Article -> Bool
prop_article a = isRight (Right . (== a) =<< deserialise (serialise a))


    
