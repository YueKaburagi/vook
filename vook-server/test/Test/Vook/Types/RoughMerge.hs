{-# LANGUAGE OverloadedStrings #-}

module Test.Vook.Types.RoughMerge where

import Vook.Types
import Vook.Types.RoughMerge

import Test.Tasty
import Test.Tasty.Hspec
import Test.Tasty.HUnit (testCase)

test_mergeSourceGroup =
  testGroup "merge source group"
  [ testCase "left additive" $
    roughMergeSourceGroup tsg1 tsg2 `shouldBe` tsg3
  , testCase "merge if" $
    ifRoughMergeSourceGroup tsg1 tsg4 `shouldBe` False
  ]



tsg1 = SourceGroup 23 (Title "abc") [AIndex 100] (Tag <$> ["tagA"]) Nothing
tsg2 = SourceGroup 42 (Title "abc") [AIndex 100] (Tag <$> ["tagB"]) Nothing
tsg3 = SourceGroup 23 (Title "abc") [AIndex 100] (Tag <$> ["tagB", "tagA"]) Nothing
tsg4 = SourceGroup 55 (Title "abc") [AIndex 100, SIndex 54] (Tag <$> ["tagA"]) Nothing
