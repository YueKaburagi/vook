{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, OverloadedLabels #-}

module Main where

import Data.Default.Class
import Control.Lens (view, use, Wrapped(..), assign, modifying)
import qualified Data.Text as Text
import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.IO as LT
import qualified Data.Text.Lazy.Builder as LTB
import qualified Data.Aeson.Text as Aeson
import qualified Data.HashMap.Strict as HashMap
import Options.Applicative

import GHC.IO.Encoding

import Vook
import Vook.State
import Vook.Types
import Vook.Types.V0
import Vook.API (runAsServer)

import Vook.Types.RoughMerge (ifRoughMergeSourceGroup)

data RunArg
  = RAOneshot
  { bookmarkUndCollectionPath :: String
  }
  | RAOneshotPriset
  { bookmarkUndPrisetPath :: String
  }
  | RAServer
  { raServerPort :: Int
  }
  deriving (Show, Eq, Ord)

runarg :: Parser RunArg
runarg = subparser
  (  command "oneshot"
    (info runargOneshot (progDesc "Run once. For import old style yaml"))
  <> command "prisets"
    (info runargOneshotPriset (progDesc "workaround>"))
  <> command "server"
    (info runargServer (progDesc "Run as server."))
  )

runargOneshot :: Parser RunArg
runargOneshot = RAOneshot
  <$> argument str (metavar "FILE" <> help "Path of old bookmark-und style collection.")
runargOneshotPriset :: Parser RunArg
runargOneshotPriset = RAOneshotPriset
  <$> argument str (metavar "FILE" <> help "Path of old bookmark-und sytle prisets.")
runargServer :: Parser RunArg
runargServer = RAServer
  <$> option auto (long "port" <> value 1357)

opts :: ParserInfo RunArg
opts = info (runarg <**> helper)
  (fullDesc
  <> progDesc "Vook server.")

main :: IO ()
main = do
  setLocaleEncoding utf8 -- use utf8 encoding
  args <- execParser opts
  prepared <- flip execStateT def $ do
    loadVookState
    case args of
      RAOneshot path -> do
        cols <- loadOldCollection path
        oatp <- loadOldArticleTypePattern "temporary/oatp.yaml"
        traverse_ (migrateOldArticle oatp) (oldCollection cols)
      RAOneshotPriset path -> do
        opris <- loadOldPrisets path
        traverse_ migratePriset opris
      _ -> pure ()
    articles <- use #articleStore
    sources <- use #sourceStore
    sgs <- use #sourceGroupStore
    pris <- use #prisetStore
    lift $ putStrLn $ "Num of Articles: " <> show (length articles)
    lift $ putStrLn $ "Num of Sources: " <> show (length sources)
    lift $ putStrLn $ "Num of Source groups: " <> show (length sgs)
    lift $ putStrLn $ "Num of Prisets: " <> show (length pris)
    let up = toV0JSONSourceGroup articles sources <$> HashMap.elems sgs
    let tb = Aeson.encodeToTextBuilder (encloseCollection up)
    let dt = LTB.fromText "const data = " <> tb
    writeFileLText "temporary/bookmark.data.js" $ LTB.toLazyText dt
    saveVookState
  case args of
    RAServer port -> do
      tv <- newTVarIO prepared
      runAsServer port tv
    _ -> pure ()
